function createNewUser() {
    var newUser = {}; 
  
    var firstName = prompt("Введіть ім'я:");
    var lastName = prompt("Введіть прізвище:");
    
    var birthDate = prompt("Введіть дату народження у форматі dd.mm.yyyy:");
  
    newUser.firstName = firstName;
    newUser.lastName = lastName;
    newUser.birthday = birthDate;
  
    newUser.getAge = function() {
      var today = new Date();
      var birthDateParts = newUser.birthday.split(".");
      var birthYear = parseInt(birthDateParts[2], 10);
      var birthMonth = parseInt(birthDateParts[1], 10) - 1; 
      var birthDay = parseInt(birthDateParts[0], 10);
  
      var age = today.getFullYear() - birthYear;
  
      if (today.getMonth() < birthMonth || (today.getMonth() === birthMonth && today.getDate() < birthDay)) {
        age--; 
      }
  
      return age;
    };
  
    newUser.getPassword = function() {
      var password = newUser.firstName.charAt(0).toUpperCase() + newUser.lastName.toLowerCase() + newUser.birthday.split(".").reverse().join("");
  
      return password;
    };
  
    return newUser;
  }
  
  var user = createNewUser();
  
  console.log(user);
  console.log("Вік користувача:", user.getAge());
  console.log("Пароль користувача:", user.getPassword());